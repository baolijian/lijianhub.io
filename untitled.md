# DigitalOcean - Initial Setup

## Ubuntu 16.4

```bash
# Setup steps
$ ssh root@server_ip
$ sudo adduser sammy
$ sudo usermod -aG sudo sammy

# add public key authenticate
$ ssh-keygen

# Firewal
$ sudo ufw status
$ sudo uft add 8080 

```

## CentOS 7

```bash
# Setup steps
$ sudo adduser sammy
$ sudo passwd sammy
$ gpasswd -a sammy wheel

# Configure SSH
$ sudo vi /etc/ssh/sshd_config
# PermitRootLogin yes, uncommented and set no
$ sudo systemctl reload sshd

# Change Time Zone
$ sudo ls -R /usr/share/zoneinfo
$ sudo cp /usr/share/zoneinfo/<TimeZone> /etc/localtime

# Firewall
# Install firewall-cmd
$ sudo yum install firewalld
$ sudo systemctl enable firewalld
$

# systemctl enable firewalld
# reboot

### verify that the service is running
# firewall-cmd --state

### getting current firewall rules
# firewall-cmd --get-default-zone
# firewall-cmd --get-active-zone

### rules are associated witht he public zone
# firewall-cmd --list-all

### Exploring alternative zones
# firewall-cmd --get-zones
# firewall-cmd --zone=home --list-all
# firewall-cmd --list-all-zones | less


### Selecting Zones for your interfaces
# firewall-cmd --zone=home --change-interface=eth0
# firewall-cmd --get-active-zones  # verify the change

### Adjusting the default zone
# firewall-cmd --set-default-zone=home

### Adding a Service to your Zones
# firewall-cmd --get-services

### services info locates
/usr/lib/firewalld/services
+/ssh.xml etc.

### Example
## Allow a traffic for interfaces in our "public" zone per session
# firewall-cmd --zone=public --add-service=http
## after testing it is working fine
# firewall-cmd --zone=public --permanent --add-service=http
# firewall-cmd --zone=public --permanent --list-services

### What if NO Appropriate Service is Available
## Open a port for your zones
# firewall-cmd --zone=public --add-port=5000/tcp
# firewall-cmd --zone=public --add-port=4990-4999/udp

## After testing, add it permanent
# firewall-cmd --zone=public --permanent --add-port=5000/tcp
# firewall-cmd --zone=public --permanent --list-ports

### Defining a Service
# cp /usr/lib/firewalld/services/ssh.xml /etc/firewalld/services/example.xml

# vi /etc/firewalld/services/example.xml

<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>exampleserice</short>
  <description>Example desc</description>
  <port protocol="tcp" port="7777"/>
  <port protocol="udp" port="8888" />
</service>

# firewall-cmd --reload
# firewall-cmd --get-services


### Create your own zones <TODO>
# firewall-cmd --permanent --new-zone=publicweb
# firewall-cmd --permanent --new-zone=priaveDNS

# firewall-cmd --permanent --get-zones
# firewall-cmd --get-zones (should not effective yet)

# firewall-cmd --reload
# firewall-cmd --get-zones (now there is)

## then you can begin assigning the appropriate services and ports
# firewall-cmd --zone=publicweb --add-service=ssh
# firewall-cmd --zone=publicweb --list-all

## then we can change interfaces over to these new zones to test them out
# firewall-cmd --zone=publicweb --change-interface=eth0
# firewall-cmd --zone=publicweb --change-interface=eth1
## after test and verified
# firewall-cmd --zone=publicweb --permanent --add-service=ssh

# systemctl restart network
# systemctl reload firewalld

# firewall-cmd --get-active-zones
# firewall-cmd --zone=publicweb --list-services

## after successfully setup own zones
# firewall-cmd --set-default-zone=publicweb



Basic Concepts in Firewalld

Zones - groups of rules using entities, basically sets of rules dictating what traffic should be allowed depneding on the level of trust you have in the networks. Network interfaces are assigned a zone to dictate the behavior that the firewall should allow.

* drop
* block
* public
* external
* internal
* dmz
* work
* home
* trusted

Rule Permanence - 
```



## Getting Super Powers

Becoming a super hero is a fairly straight forward process:

```
$ give me super-powers
```

{% hint style="info" %}
 Super-powers are granted randomly so please submit an issue if you're not happy with yours.
{% endhint %}

Once you're strong enough, save the world:

```
// Ain't no code for that yet, sorry
echo 'You got to trust me on this, I saved the world'
```



